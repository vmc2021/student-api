package com.delta.students.services;

import java.util.List;

import com.delta.students.models.Student;

public interface StudentService {

    List<Student> findAll();

    // method to SAVE an object of our model - "Student"
    Student saveStudent(Student student);// save() with data type of Student

    // method to find an object by the id
    Student findById(Long id);

    // delete method
    void delete(Long id);



}
