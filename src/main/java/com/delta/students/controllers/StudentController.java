package com.delta.students.controllers;

import java.util.List;

import com.delta.students.models.Student;
import com.delta.students.services.StudentService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/v1")
public class StudentController {
    
    //bring post service, injecting our post service to controller
    @Autowired
    StudentService sService;

    // get method to Get the data from our database -> table name
    @GetMapping("/students")
    public ResponseEntity<List<Student>> get() {
        List<Student> students = sService.findAll();

        return new ResponseEntity<List<Student>>(students, HttpStatus.OK);
    }
    //post method to POST new data to out database
    @PostMapping("/students")
    public ResponseEntity<Student> saveStudent(@RequestBody Student student) {
        Student newStudent = sService.saveStudent(student);

        return new ResponseEntity<Student>(newStudent, HttpStatus.OK);
    }

     //create a get method to Get the data of an individual Post object
    //based off of the id property
    @GetMapping("/students/{id}")
    public ResponseEntity<Student> getPost(@PathVariable("id") Long id) {
        Student viewStudent = sService.findById(id);

        return new ResponseEntity<Student>(viewStudent, HttpStatus.OK);
    }

    
    //delete method to DELETE the data of an individual post object
    // based off of the objects id
    @DeleteMapping("/students/{id}")
    public ResponseEntity<String> delete(@PathVariable("id") Long id){
        sService.delete(id);
        return new ResponseEntity<String>("Student is deleted successfully!", HttpStatus.OK);
    }



}
