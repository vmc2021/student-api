package com.delta.students.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "students")
@Getter
@Setter
public class Student {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    
    private long id;
    private String firstName;
    private String lastName;
    private String studentID;
    private String email;
    private long gradeLevel;
    private String dateOfBirth;
    private String emergencyContactNumber;

    

}
